let inputUserNum = prompt(`Enter a number:`);
console.log(`The number your provided is ${inputUserNum}.`);

for(inputUserNum; 0 <= inputUserNum; inputUserNum--) {
	
	if(isNaN(inputUserNum) || typeof parseInt(inputUserNum) !== `number`) {
		(console.log(`Not a number.`))
	} else {
		if((inputUserNum % 10 == 0) && (inputUserNum != 50)){
			console.log(`The number is divisible by 10. Skipping the number`)
		}
		
		if((inputUserNum % 5 == 0) != (inputUserNum % 10 == 0)) {
			console.log(inputUserNum)
		}
		if(inputUserNum <= 50) {
			console.log(`The current value is at 50. Terminating the loop.`)
			break;
		}
	}
}

let word = `supercalifragilisticexpialidocious`;
console.log(word);
let vowels = [`a`, `e`,  `i`,  `o`, `u`];
let conso = ``;

for (let letters of word) {
	if(vowels.indexOf(letters) > -1) {
		continue;
	}
	if(vowels.indexOf(letters) === -1) {
		conso += letters;
	}
}

console.log(conso);
